# Desenvolvimento
Instale as dependências e inicie o servidor

```sh
$ git clone https://mechamoandrey@bitbucket.org/mechamoandrey/sass.git
$ cd sass
$ npm run dev 
$ npm run server 
```
# Produção
 
Roda o script build e abre um server

```sh
$ npm start
```
