const gulp = require('gulp')
const sass = require('gulp-sass')

gulp.task('buildSass', () =>
    gulp.src('./src/sass/**/*.sass')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest('./public/css'))
)

gulp.task('watch', () => 
    gulp.watch('./src/sass/**/*.sass', ['buildSass'])
)